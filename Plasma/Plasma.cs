﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoGame.Extended;
using MonoGame.Extended.Input.InputListeners;
using SharpNoise;
using SharpNoise.Builders;
using SharpNoise.Modules;
using System;

namespace Plasma {
    public class Game1 : Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Camera2D camera;
        KeyboardListener keyboardListener;
        RenderTarget2D renderTarget;
        Random random = new Random();

        SpriteFont font;

        MouseState currentMouseState;

        NoiseCube noiseCube = null;
        NoiseMap midNoiseMap;

        int width = 512;
        int height = 512;
        int depth = 128;

        int numSteps = 16;
        int currentStep = 0;

        int generation = 0;

        public Game1() {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            
            graphics.PreferredBackBufferHeight = 800;
            graphics.PreferredBackBufferWidth = 1280;

            IsFixedTimeStep = false;

            noiseCube = new NoiseCube();
            var builder = new LinearNoiseCubeBuilder {
                DestNoiseCube = noiseCube,
                SourceModule = new Turbulence {
                    Source0 = new SharpNoise.Modules.Blend {
                        Control = new Cylinders { Frequency = 2 },
                        Source1 = new RidgedMulti { Seed = random.Next() },
                        Source0 = new Invert {
                            Source0 = new Billow { Seed = random.Next() },
                        }
                    },
                    Roughness = 4
                }
            };
            
            builder.SetBounds(-1, 1, -1, 1, -1, 1);
            builder.SetDestSize(width, height, depth);
            builder.Build();
        }

        protected override void Initialize() {
            camera = new Camera2D(GraphicsDevice) { Position = new Vector2(-405, -143), Zoom = 1.55f };

            renderTarget = new RenderTarget2D(GraphicsDevice, width, height,
                false, GraphicsDevice.PresentationParameters.BackBufferFormat, DepthFormat.None, 0, RenderTargetUsage.PreserveContents);

            keyboardListener = new KeyboardListener();

            keyboardListener.KeyPressed += KeyboardListener_KeyPressed;
            
            base.Initialize();
        }

        private void KeyboardListener_KeyPressed(object sender, KeyboardEventArgs e) {
            switch (e.Key) {
                case Keys.Up:
                case Keys.W:
                    camera.Move(-Vector2.UnitY * 8 / camera.Zoom);
                    break;
                case Keys.Down:
                case Keys.S:
                    camera.Move(Vector2.UnitY * 8 / camera.Zoom);
                    break;
                case Keys.Left:
                case Keys.A:
                    camera.Move(-Vector2.UnitX * 8 / camera.Zoom);
                    break;
                case Keys.Right:
                case Keys.D:
                    camera.Move(Vector2.UnitX * 8 / camera.Zoom);
                    break;
                case Keys.PageDown:
                    camera.ZoomIn(.025f);
                    break;
                case Keys.PageUp:
                    camera.ZoomOut(.025f);
                    break;
            }

            Console.WriteLine($"({camera.Position.X}, {camera.Position.Y}) @ {camera.Zoom}");
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = Content.Load<SpriteFont>("Font");
        }

        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            keyboardListener.Update(gameTime);

            currentMouseState = Mouse.GetState();

            midNoiseMap = new NoiseMap(width, height);
            var genMod = generation % (depth - 1);
            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    var currVal = noiseCube.GetValue(i, j, genMod);
                    var nextVal = noiseCube.GetValue(i, j, genMod + 1);
                    midNoiseMap.SetValue(i, j, MathHelper.LerpPrecise(currVal, nextVal, (float)currentStep / (float)numSteps));
                }
            }
            currentStep++;

            if (currentStep == numSteps) {
                currentStep = 0;
                generation++;
            }
            
            base.Update(gameTime);
        }
        
        private void renderNoiseMap(NoiseMap noiseMap) {
            //noiseMap.MinMax(out var min, out var max);
            var min = -1;
            var max = 1;
            GraphicsDevice.SetRenderTarget(renderTarget);
            spriteBatch.Begin();

            for (int i = 0; i < width; i++) {
                for (int j = 0; j < height; j++) {
                    var val = noiseMap.GetValue(i, j);
                    val = val.Map(0, 360, min, max);
                    spriteBatch.DrawPoint(i, j, new HslColor(val, .75f, .5f).ToRgb());
                }
            }

            spriteBatch.End();
            GraphicsDevice.SetRenderTarget(null);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.Black);
            renderNoiseMap(midNoiseMap);

            spriteBatch.Begin(transformMatrix: camera.GetViewMatrix());
            spriteBatch.Draw(renderTarget, Vector2.Zero, Color.White);
            spriteBatch.End();

            spriteBatch.Begin();
            spriteBatch.DrawString(font, $"Step: {currentStep}/{numSteps}\r\nGeneration: {generation}", Vector2.Zero, Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
