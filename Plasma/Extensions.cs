﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Plasma {
    static class Extensions {
        public static float Map(this float oldValue, float newMin, float newMax, float oldMin = 0f, float oldMax = 1f) {
            var OldRange = (oldMax - oldMin);
            var NewValue = 0f;
            if (OldRange == 0) {
                NewValue = newMin;
            } else {
                var NewRange = (newMax - newMin);
                NewValue = (((oldValue - oldMin) * NewRange) / OldRange) + newMin;
            }

            return NewValue;
        }

        public static T RandomEnumValue<T>() {
            var v = Enum.GetValues(typeof(T));
            return (T)v.GetValue(new Random().Next(v.Length));
        }
    }
}
